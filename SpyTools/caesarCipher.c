#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

char performShift(char original, int shift);

int main(int argc, char *args[]){
    int shift;
    if(argc != 3 || (shift = atoi(args[2])) == 0){
        printf("Usage: ./ceasarChipher <file containing string to be decoded> <non-zero shift number>\n");
        exit(0);
    }

    char c;
    
    FILE* codeFile = fopen(args[1], "r");

    if(codeFile){
        while((c = fgetc(codeFile)) != EOF){
            if((int)c <= 31 || (int)c >= 127){
                continue;
            }else{
                printf("%c", performShift(c, shift) );
            }
        }
    }else{
        printf("%s does not exist", args[1]);
        exit(0);
    }
    fclose(codeFile);


}

char performShift(char original, int shift){
    int base = original - 32;
    int newbase = (base + shift) % 95;
    return (char)(newbase + 32);
}