const express = require("express");
const bodyParser = require("body-parser");
const app = express();
//const session = require("express-session")
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
const password = "spectre"

/// SERVER ///

app.get("/", (req, res) => {
    console.log("GET /");
    res.sendFile(__dirname + "/martinsbakery.html");
});

app.get("/evillogin", (req, res) => {
    console.log("GET /");
    res.sendFile(__dirname + "/evillogin.html");
});

app.get("/message", (req, res) => {
    console.log("GET /message");
    res.sendFile(__dirname + "/message.html");
});

app.get("/style.css", (req, res) => {
    console.log("GET /style.css");
    res.sendFile(__dirname + "/style.css");
});

app.get("/evilstyle.css", (req, res) => {
    console.log("GET /evilstyle.css");
    res.sendFile(__dirname + "/evilstyle.css");
});

/*app.get("/message.css", (req, res) => {
    console.log("GET /message.css");
    res.sendFile(__dirname + "/message.css");
});*/


app.post("/api/login", (req, res) => {
    console.log(req.body.password);
    if(req.body.password == password) {
        res.sendFile(__dirname + "/message.html");
    } else {
        res.send("Invalid password");
    }
});

const port = process.getuid()
app.listen(port, () => console.log(`Listening on port ${port}`));