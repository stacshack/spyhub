#! /usr/bin/env python3

def main():
    for line in open("assignment.txt"):
        print(line)

    address = input("Where is the agent being kept?")
    while address != "36 main street":
        print("An operation was conducted to retrieve the agent from ", address, " and they were not found")
        address = input("Where is the agent being kept?")

    print("Congratulations - the agent has been safely retrieved")

if __name__ == "__main__":
    main()
        